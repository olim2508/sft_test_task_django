from django.db import models


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True, verbose_name='Дата создания')

    class Meta:
        abstract = True


class Manufacturer(BaseModel):
    name = models.CharField(max_length=255, verbose_name='Наименование производителя')

    class Meta:
        verbose_name = 'Производитель'
        verbose_name_plural = 'Производители'


class Product(BaseModel):
    name = models.CharField(max_length=255, verbose_name='Наименование товара')
    manufacturer = models.ForeignKey("main.Manufacturer", on_delete=models.CASCADE, related_name='products',
                                     verbose_name='Производитель')
    credit_application = models.ForeignKey("main.CreditApplication", on_delete=models.CASCADE,
                                           related_name='products_related', verbose_name='Заявка')

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class Contract(BaseModel):
    name = models.CharField(max_length=255, verbose_name='Наименование контракта')

    class Meta:
        verbose_name = 'Контракт'
        verbose_name_plural = 'Контракты'


class CreditApplication(BaseModel):
    contract = models.OneToOneField("main.Contract", on_delete=models.CASCADE, related_name='credit_application',
                                    verbose_name='Контракт')

    class Meta:
        verbose_name = 'Кредитная заявка'
        verbose_name_plural = 'Кредитные заявки'

