from django.urls import path

from main.views import UniqueManufacturersView

app_name = 'main'

urlpatterns = [
    path('unique-manufacturers/<int:pk>/', UniqueManufacturersView.as_view(), name='unique-manufacturers-detail'),
]

