from rest_framework import generics
from rest_framework.response import Response
from .models import CreditApplication


class UniqueManufacturersView(generics.RetrieveAPIView):
    queryset = CreditApplication.objects.select_related('contract')\
        .prefetch_related('products_related__manufacturer')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        unique_manufacturer_ids = instance.products_related.values_list("manufacturer_id", flat=True).distinct()
        return Response({'unique_manufacturer_ids': unique_manufacturer_ids})
