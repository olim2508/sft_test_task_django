from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from .models import Manufacturer, Product, Contract, CreditApplication


class ProductViewSetTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

        self.manufacturer_1 = Manufacturer.objects.create(name='Manufacturer 1')
        self.manufacturer_2 = Manufacturer.objects.create(name='Manufacturer 2')

        self.contract_1 = Contract.objects.create(name='Contract 1')
        self.credit_application_1 = CreditApplication.objects.create(contract=self.contract_1)
        self.product_1 = Product.objects.create(name='Product 1', manufacturer=self.manufacturer_1,
                                                credit_application=self.credit_application_1)

        self.contract_2 = Contract.objects.create(name='Contract 2')
        self.credit_application_2 = CreditApplication.objects.create(contract=self.contract_2)
        self.product_2 = Product.objects.create(name='Product 2', manufacturer=self.manufacturer_2,
                                                credit_application=self.credit_application_2)

    def test_retrieve_unique_manufacturer_ids(self):
        response = self.client.get(f'/api/v1/main/unique-manufacturers/{self.contract_1.id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertIn(self.manufacturer_1.id, response.data['unique_manufacturer_ids'])
        self.assertNotIn(self.manufacturer_2.id, response.data['unique_manufacturer_ids'])

        response = self.client.get(f'/api/v1/main/unique-manufacturers/{self.contract_2.id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn(self.manufacturer_2.id, response.data['unique_manufacturer_ids'])
        self.assertNotIn(self.manufacturer_1.id, response.data['unique_manufacturer_ids'])

