# Тестовое задание СФТ
## Сделал - [Olim Rakhmatov](https://www.linkedin.com/in/olim-rakhmatov/)


### Для запуска проекта выполните следующие команды:
    docker-compose up -d --build

Это запустит сервер по адресу http://localhost:8000.

### Тестирование API
Тесты для API, описанные в задании, находятся в файле ``main/tests.py``. Для запуска тестов выполните команду:
    
    docker-compose exec backend python manage.py test
